﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmconfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnsaveserver = New System.Windows.Forms.Button()
        Me.txtdatabase = New System.Windows.Forms.TextBox()
        Me.txtpassword = New System.Windows.Forms.TextBox()
        Me.txtid = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtserver = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.txtlicense = New System.Windows.Forms.TextBox()
        Me.txtcompany = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(4, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(324, 262)
        Me.TabControl1.TabIndex = 1
        Me.TabControl1.Tag = ""
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnsaveserver)
        Me.TabPage1.Controls.Add(Me.txtdatabase)
        Me.TabPage1.Controls.Add(Me.txtpassword)
        Me.TabPage1.Controls.Add(Me.txtid)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.txtserver)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(316, 236)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Server Configuration"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnsaveserver
        '
        Me.btnsaveserver.Location = New System.Drawing.Point(216, 186)
        Me.btnsaveserver.Name = "btnsaveserver"
        Me.btnsaveserver.Size = New System.Drawing.Size(75, 23)
        Me.btnsaveserver.TabIndex = 8
        Me.btnsaveserver.Text = "Save"
        Me.btnsaveserver.UseVisualStyleBackColor = True
        '
        'txtdatabase
        '
        Me.txtdatabase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.Payroll_System.My.MySettings.Default, "mdatabase", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtdatabase.Location = New System.Drawing.Point(78, 101)
        Me.txtdatabase.Name = "txtdatabase"
        Me.txtdatabase.Size = New System.Drawing.Size(140, 20)
        Me.txtdatabase.TabIndex = 7
        Me.txtdatabase.Text = Global.Payroll_System.My.MySettings.Default.mdatabase
        '
        'txtpassword
        '
        Me.txtpassword.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.Payroll_System.My.MySettings.Default, "mpassword", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtpassword.Location = New System.Drawing.Point(78, 70)
        Me.txtpassword.Name = "txtpassword"
        Me.txtpassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtpassword.Size = New System.Drawing.Size(140, 20)
        Me.txtpassword.TabIndex = 6
        Me.txtpassword.Text = Global.Payroll_System.My.MySettings.Default.mpassword
        '
        'txtid
        '
        Me.txtid.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.Payroll_System.My.MySettings.Default, "musername", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtid.Location = New System.Drawing.Point(78, 45)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(140, 20)
        Me.txtid.TabIndex = 5
        Me.txtid.Text = Global.Payroll_System.My.MySettings.Default.musername
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 101)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Database"
        '
        'txtserver
        '
        Me.txtserver.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.Payroll_System.My.MySettings.Default, "mserver", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtserver.Location = New System.Drawing.Point(78, 15)
        Me.txtserver.Name = "txtserver"
        Me.txtserver.Size = New System.Drawing.Size(140, 20)
        Me.txtserver.TabIndex = 3
        Me.txtserver.Text = Global.Payroll_System.My.MySettings.Default.mserver
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 70)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Password"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "ID"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Server"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnsave)
        Me.TabPage2.Controls.Add(Me.txtlicense)
        Me.TabPage2.Controls.Add(Me.txtcompany)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(316, 236)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "License Configuration"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(209, 182)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(75, 23)
        Me.btnsave.TabIndex = 4
        Me.btnsave.Text = "Save"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'txtlicense
        '
        Me.txtlicense.Location = New System.Drawing.Point(63, 87)
        Me.txtlicense.Name = "txtlicense"
        Me.txtlicense.Size = New System.Drawing.Size(100, 20)
        Me.txtlicense.TabIndex = 3
        '
        'txtcompany
        '
        Me.txtcompany.Location = New System.Drawing.Point(63, 43)
        Me.txtcompany.Name = "txtcompany"
        Me.txtcompany.Size = New System.Drawing.Size(100, 20)
        Me.txtcompany.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "License"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Company"
        '
        'frmconfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(328, 264)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmconfig"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnsave As Button
    Friend WithEvents txtlicense As TextBox
    Friend WithEvents txtcompany As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtid As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtserver As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btnsaveserver As Button
    Friend WithEvents txtdatabase As TextBox
    Friend WithEvents txtpassword As TextBox
End Class
