﻿Public Class frmlicense
    Private Sub btnsave_Click(sender As Object, e As EventArgs) Handles btnsave.Click
        txtdatabase.Text = My.Settings.mdatabase
        txtPassword.Text = My.Settings.mpassword
        txtserver.Text = My.Settings.mserver
        txtuser.Text = My.Settings.musername
        My.Settings.Save()
        MsgBox("Server Settings Saved", MsgBoxStyle.Information, MsgBoxResult.Ok)
        My.Settings.Reload()
        Application.Restart()

    End Sub

    Private Sub frmlicense_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        My.Settings.Reload()

    End Sub
End Class