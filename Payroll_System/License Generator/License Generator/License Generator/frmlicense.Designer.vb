﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmlicense
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtserver = New System.Windows.Forms.TextBox()
        Me.txtuser = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtdatabase = New System.Windows.Forms.TextBox()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(-1, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Server Configuration"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Server"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "User"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Password"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 116)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Database"
        '
        'txtserver
        '
        Me.txtserver.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.License_Generator.My.MySettings.Default, "mserver", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtserver.Location = New System.Drawing.Point(63, 29)
        Me.txtserver.Name = "txtserver"
        Me.txtserver.Size = New System.Drawing.Size(100, 20)
        Me.txtserver.TabIndex = 5
        Me.txtserver.Text = Global.License_Generator.My.MySettings.Default.mserver
        '
        'txtuser
        '
        Me.txtuser.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.License_Generator.My.MySettings.Default, "musername", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtuser.Location = New System.Drawing.Point(63, 55)
        Me.txtuser.Name = "txtuser"
        Me.txtuser.Size = New System.Drawing.Size(100, 20)
        Me.txtuser.TabIndex = 6
        Me.txtuser.Text = Global.License_Generator.My.MySettings.Default.musername
        '
        'txtPassword
        '
        Me.txtPassword.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.License_Generator.My.MySettings.Default, "mpassword", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtPassword.Location = New System.Drawing.Point(63, 81)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(100, 20)
        Me.txtPassword.TabIndex = 7
        Me.txtPassword.Text = Global.License_Generator.My.MySettings.Default.mpassword
        '
        'txtdatabase
        '
        Me.txtdatabase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.License_Generator.My.MySettings.Default, "mdatabase", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtdatabase.Location = New System.Drawing.Point(63, 113)
        Me.txtdatabase.Name = "txtdatabase"
        Me.txtdatabase.Size = New System.Drawing.Size(100, 20)
        Me.txtdatabase.TabIndex = 8
        Me.txtdatabase.Text = Global.License_Generator.My.MySettings.Default.mdatabase
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(99, 141)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(75, 23)
        Me.btnsave.TabIndex = 9
        Me.btnsave.Text = "Save"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'frmlicense
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(175, 168)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.txtdatabase)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtuser)
        Me.Controls.Add(Me.txtserver)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmlicense"
        Me.Text = "frmlicense"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtserver As TextBox
    Friend WithEvents txtuser As TextBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents txtdatabase As TextBox
    Friend WithEvents btnsave As Button
End Class
